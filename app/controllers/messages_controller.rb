require 'streamer/sse'
class MessagesController < ApplicationController
  include ActionController::Live
  def index
    @messages = Message.all
  end

  def create
    response.headers['Content-Type'] = 'text/javascript'
    @message = params.require(:message).permit(:name, :content)
    $redis.publish('messages.create', @message.to_json)
    render nothing: true
  end

  def events
    response.headers['Content-Type'] = 'text/event-stream'
    sse = Streamer::SSE.new(response.stream)
    redis = Redis.new
    redis.subscribe('messages.create') do |on|
      on.message do |event, data|
        sse.write(data, event: 'messages.create')
      end
    end
    render nothing: true
  rescue IOError
    puts "==================================="
    # Client disconnected
  ensure
    redis.quit
    sse.close
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:content)
    end
end
